<?php

$hostname = 'root';
$password = '';
$servername = 'localhost';
$dbname = 'todo';

$conn = new mysqli($servername, $hostname, $password, $dbname);

if ($conn->connect_error) {
    die("connection failed: ".$conn->connect_error);
}

?>